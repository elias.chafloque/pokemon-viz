# Pokémon Exploratory Tool

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

This project takes data from this [repository](https://github.com/thetylerwolf/pokemon_cards)

[Video Explanation](https://www.youtube.com/watch?v=B88lp2lsjL8)

[Live version](https://pokemon.ixti.fi)

Main idea in a [Figma protoype](https://www.figma.com/proto/y28Jod3BzEYvjKpiCnb5Rh/Pok%C3%A9mon-Cards-Exploration-Tool?node-id=23%3A26&scaling=min-zoom&page-id=0%3A1&starting-point-node-id=1%3A25&show-proto-sidebar=1)

## Introduction

The idea of this challenge is to create an exploratory tool for Pokemon card collectors.
They would have a better visibility of their collection by filtering and visualizing from many angles.

The current project is a fast prototype (1-2 days) using React and D3. It gives an idea of a tool with exploratory purposes.

#### Why designing/creating a tool?

My first approach would have been to interview passionate people about the game. They would have been able to tell me which angle would have been interesting to focus and analyze.

But I decided to create a more generic tool and it was a good way for self learning. This would also allow many type of players to explore their own collection as they have different needs.

## Run the Tool

You can run locally the project by executing the script `npm start`.

You have the live version [here](https://pokemon.ixti.fi) (using aws amplify).

## About the Design

The idea is to have initially a Filters panel (with many selectors), and this will affect on the KPIs, and visualization.

The visualization itself can work as an extra selector that does not affect on the Filters panel but affects on the Cards output
![design](design.png)

## About Technology

For this project I am using...

- React as main framework
- Ant Design as UI library (mainly for the filters)
- Lodash for array calculations
- D3 for visualization (scatterplot)

## About Data Processing

Data was not immediately in use, so had to enrich it with another file that contains the sets. I had also to precalculate some metrics (most of them counting), and also some attributes like subtypes went from array to single string (a Pokémon can have to types, so the combination is a type itself).

Cards were repeated, so I removed the duplicates and added a counter.
All these would have been done in the backend in a real situation.

## About Visualization

You can interact with the visualization by choosing the axis you desire.

I decided to do a Scatterplot to visualize the cards, but then I realized that there are many cards with same values, so I decided to group them according to the axes. Bigger bubbles will mean more cards. Bubbles can be selected but as an improvement I would like to use brushing so I can select many groups.

As it is a fast prototype labels, KPIs, tooltips, interactions were not developed. Code is also not good but I wanted to do something functional as fast as possible to give an idea.

I thought also about adding a bar chart as a support visualization.
![barchart](barchart.png)

## Conclusion

Many things to improve and add, considering design but also in code. But definitely a great challenge and I got really motivated in doing it as a finished product. Many ideas came in as developing this project.
